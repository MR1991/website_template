# Website_Template

This page will describe how to set up a new flask based website within 1 hour.

## How to create template

Design is shown in the picture below:  
![image](https://gitlab.com/MR1991/website_template/wikis/uploads/021b18f12229fba2a5dd3004b91d023a/image.png)

## Local, Development (Windows)
Download anaconda & pycharm
Create environment in anaconda
Use anaconda to install wheel flask
git clone this repository
Open repo in pycharm as new project.
File -> settings -> project/project_interpreter -> select path to anaconda environment something like
"C:\Users\User\Documents\Anaconda\envs\env_name\python.exe"

## Local, Development/Production (Linux)
Git clone repository
chmod +x on prepare_vm.sh and prepare_prod_env.sh
Run sh prepare_vm.sh
Run sh prepare_prod_env.sh
Run python

## Remote
### Set up Compute Engine
Go to google cloud and create a new project.
Within the project create a new compute engine with settings:
```
1v CPU  3.75 GB memory
Boot disk Debian 9
Allow full access to all Cloud APIs
Allow HTTP traffic
Allow HTTPS traffic
```
Go to External IP Addresses, change to static.

### Install packages
This installs Python 3.7, Git(version control), flask (application), gunicorn (WSGI server) and nginx (Web Server).
Drop contents of prepare_environement.sh in another file and do the same.
IMPORTANT: Replace $USER by your username (same as home/username/ path)

website.service sets up a startup service.

### Project folder structure
If you cloned the entire repository you will have the following file structure:
```
run.py	This is the file that is invoked to start up a development server. It gets a copy of the app from your package and runs it. This won’t be used in production, but it will see a lot of mileage in development.
requirements.txt	This file lists all of the Python packages that your app depends on. You may have separate files for production and development dependencies.
wsgi.py	 This is an entrypoint to flask app. This will tell our Gunicorn server how to interact with the application.
config.py	This file contains most of the configuration variables that your app needs.
/instance/config.py	This file contains configuration variables that shouldn’t be in version control. This includes things like API keys and database URIs containing passwords. This also contains variables that are specific to this particular instance of your application. For example, you might have DEBUG = False in config.py, but set DEBUG = True in instance/config.py on your local machine for development. Since this file will be read in after config.py, it will override it and set DEBUG = True.
/yourapp/	This is the package that contains your application.
/yourapp/__init__.py	This file initializes your application and brings together all of the various components.
/yourapp/views.py	This is where the routes are defined. It may be split into a package of its own (yourapp/views/) with related views grouped together into modules.
/yourapp/models.py	This is where you define the models of your application. This may be split into several modules in the same way as views.py.
/yourapp/static/	This directory contains the public CSS, JavaScript, images and other files that you want to make public via your app. It is accessible from yourapp.com/static/ by default.
/yourapp/templates/	This is where you’ll put the Jinja2 templates for your app.
```


### Set up HTTPS
https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-ubuntu-18-04


### Optional: set up atom:

On google cloud go to Firewall rules. Ingress, Allow, Source Ip ranges: 0.0.0.0/0, Specified protocols and ports, tcp: 52698.

On local computer:
* Install google cloud SDK shell locally https://dl.google.com/dl/cloudsdk/channels/rapid/GoogleCloudSDKInstaller.exe
* Install putty https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html
* Install atom from https://atom.io/
* In Atom on the homepage "install packages -> remote atom", then "packages -> remote atom -> start server".
* Start up the google cloud SDK shell. Do the below two things. Replace project, user@x and zone.

```
gcloud init
gcloud compute --project "website-template-226613" ssh --zone "us-east1-b" "mauricerichard1991@website" -- -R 52698:localhost:52698 -L8000:localhost:8000
```
On compute engine:
```
sudo curl -o /usr/local/bin/rmate https://raw.githubusercontent.com/aurora/rmate/master/rmate
sudo chmod +x /usr/local/bin/rmate
sudo mv /usr/local/bin/rmate /usr/local/bin/ratom
```
Then use sudo ratom example.txt command on compute engine to edit files locally.



```
import os
from flask import Flask, send_file
app = Flask(__name__)


@app.route("/hello")
def hello():
    return "Hello World from Flask"


@app.route("/")
def main():
    index_path = os.path.join(app.static_folder, 'index.html')
    return send_file(index_path)


# Everything not declared before (not a Flask route / API endpoint)...
@app.route('/<path:path>')
def route_frontend(path):
    # ...could be a static file needed by the front end that
    # doesn't use the `static` path (like in `<script src="bundle.js">`)
    file_path = os.path.join(app.static_folder, path)
    if os.path.isfile(file_path):
        return send_file(file_path)
    # ...or should be handled by the SPA's "router" in front end
    else:
        index_path = os.path.join(app.static_folder, 'index.html')
        return send_file(index_path)


if __name__ == "__main__":
    # Only for debugging while developing
    app.run(host='0.0.0.0', debug=True, port=80)
```
