virtualenv --python python3.7 venv
source venv/bin/activate
pip install wheel
pip install cookiecutter
pip install flask 
pip install gunicorn
pip install uwsgi
sudo dd of=/etc/systemd/system/website.service << EOF
[Unit]
Description=Gunicorn instance to serve website
After=network.target

[Service]
User=${USER}
Group=www-data
WorkingDirectory=/home/${USER}/website
Environment="PATH=/home/${USER}/website/venv/bin"
ExecStart=/home/${USER}/website/venv/bin/gunicorn --workers 3 --bind unix:website.sock -m 007 wsgi:app

[Install]
WantedBy=multi-user.target
EOF
sudo systemctl daemon-reload
sudo systemctl start website
sudo systemctl enable website
sudo cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.backup
sudo dd of=/etc/nginx/sites-available/default << EOF
server {
    listen 80 default_server;
    listen [::]:80 default_server;

    root /var/www/html;

    index index.html index.htm index.nginx-debian.html;

    server_name _;

    location / {
        include proxy_params;
        proxy_pass http://unix:/home/${USER}/website/website.sock;
    }
}
EOF
sudo ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/
sudo nginx -t
sudo systemctl restart nginx
sudo cat > ~/.bash_aliases <<EOF
alias alias1='sudo nano ~/.bash_aliases'
alias alias2='sudo source ~/.bashrc'
EOF
cd ~/
sh .bashrc
sudo mkdir /etc/systemd/system/nginx.service.d
sudo dd of=/etc/systemd/system/nginx.service.d/override.conf << EOF
[Service]
ExecStartPost=/bin/sleep 0.1
EOF
sudo systemctl daemon-reload
echo "done"
