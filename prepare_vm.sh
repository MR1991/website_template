sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get install -y virtualenv
sudo apt-get install -y python-pip
sudo apt-get dist-upgrade -y
sudo apt-get install -y build-essential checkinstall python-dev python-setuptools python-pip python-smbus
sudo apt-get install -y libffi-dev libreadline-gplv2-dev libncursesw5-dev libssl-dev \
    libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev zlib1g-dev openssl 
cd /usr/src
sudo wget https://www.python.org/ftp/python/3.7.0/Python-3.7.0.tgz
sudo tar xzf Python-3.7.0.tgz
cd Python-3.7.0
sudo ./configure --enable-optimizations
sudo make altinstall
python3.7 -V
sudo apt-get install -y git
sudo apt-get install -y gunicorn
sudo apt-get install -y nginx