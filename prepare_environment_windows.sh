virtualenv --python python3.7 venv
source venv/bin/activate
pip install wheel
pip install cookiecutter
pip install flask 
pip install gunicorn
pip install uwsgi